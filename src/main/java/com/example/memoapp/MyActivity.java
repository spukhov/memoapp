package com.example.memoapp;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Intent;
import android.view.View;
import android.widget.*;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

public class MyActivity extends ListActivity {

    private ProgressDialog pDialog;

    // URL to get contacts JSON
    private static String url = "http://192.168.1.130:8080/memo/api/notes";

    // JSON Node names
    private static final String TAG_ID = "id";
    private static final String TAG_TITLE = "title";
    private static final String TAG_TIME = "time";
    private static final String TAG_TEXT = "text";

    // contacts JSONArray
    JSONArray notes;
    DBHelper dbHelper;
    Button button;

    // Hashmap for ListView
    ArrayList<HashMap<String, String>> noteList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        noteList = new ArrayList<HashMap<String, String>>();

        ListView lv = getListView();

        dbHelper = new DBHelper(getApplicationContext());
        // Calling async task to get json
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*BaseAdapter adapter = new SimpleAdapter(
                        MyActivity.this, noteList,
                        R.layout.list_item, new String[] { TAG_TITLE, TAG_TIME,
                        TAG_TEXT }, new int[] { R.id.title,
                        R.id.time, R.id.text });

                setListAdapter(adapter);
                adapter.notifyDataSetChanged();*/
                MyActivity.this.recreate();
            }
        });

        new GetNotes().execute();
    }

    public void getUserName(){

        List<String> list = dbHelper.selectAll();
        Log.e("pooh - usernames", list.toString());
        if (list.isEmpty()){
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }

    }
    /**
     * Async task class to get json by making HTTP call
     * */
    private class GetUserId extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            getUserName();
            return null;
        }
    }

     private class GetNotes extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(MyActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            List list = dbHelper.selectAll();

            if (list.isEmpty()){
                getUserName();
            } else {
                // Making a request to url and getting response
                String jsonStr = sh.makeServiceCall(url + "/" + list.get(0), ServiceHandler.GET);

                Log.d("Response: ", "> " + jsonStr);

                if (jsonStr != null) {
                    try {
                        notes = new JSONArray(jsonStr);

                        // looping through All Contacts
                        for (int i = 0; i < notes.length(); i++) {
                            JSONObject c = notes.getJSONObject(i);

                            String id = c.getString(TAG_ID);
                            String title = c.getString(TAG_TITLE);
                            String time = c.getString(TAG_TIME);
                            String text = c.getString(TAG_TEXT);

                            // Phone node is JSON Object

                            // tmp hashmap for single contact
                            HashMap<String, String> notes = new HashMap<String, String>();

                            // adding each child node to HashMap key => value
                            notes.put(TAG_ID, id);
                            notes.put(TAG_TITLE, title);
                            notes.put(TAG_TIME, time);
                            notes.put(TAG_TEXT, text);

                            // adding contact to contact list
                            noteList.add(notes);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e("ServiceHandler", "Couldn't get any data from the url");
                }
            }



            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            ListAdapter adapter = new SimpleAdapter(
                    MyActivity.this, noteList,
                    R.layout.list_item, new String[] { TAG_TITLE, TAG_TIME,
                    TAG_TEXT }, new int[] { R.id.title,
                    R.id.time, R.id.text });

            setListAdapter(adapter);
        }

    }

}
