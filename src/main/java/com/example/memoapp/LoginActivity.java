package com.example.memoapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class LoginActivity extends Activity {
    EditText email;
    EditText password;
    Button button;
    private ProgressDialog pDialog;
    DBHelper dbHelper;

    String url = Utils.API_URL+"/user/validate/";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        dbHelper = new DBHelper(getApplicationContext());

        email  = (EditText) findViewById(R.id.emailText);
        password  = (EditText) findViewById(R.id.passwordText);
        button = (Button) findViewById(R.id.button);
    }

    public void onStart() {
        super.onStart();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String emailStr = email.getText().toString();
                String passStr = password.getText().toString();

                new ValidateUser().execute();

            }
        });
    }

    private class ValidateUser extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(LoginActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("email", email.getText().toString()));
            params.add(new BasicNameValuePair("pass", password.getText().toString()));

            // Making a request to url and getting response
            String response = sh.makeServiceCall(url, ServiceHandler.POST, params);

            Log.d("Pooh: Response: ", "> " + response);

            if (response.equalsIgnoreCase("false")){
                email.setText("");
                password.setText("");
                Toast toast = Toast.makeText(getApplicationContext(), "Validation failed!", 500);
                toast.show();

            }   else {
                dbHelper.insert(response);
                Log.e("POOOOH","response: " + response);
                Intent intent = new Intent(getApplicationContext(), MyActivity.class);
                startActivity(intent);
            }

            return null;
        }

    }
}